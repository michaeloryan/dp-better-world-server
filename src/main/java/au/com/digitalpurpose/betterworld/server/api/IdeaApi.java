package au.com.digitalpurpose.betterworld.server.api;

import au.com.digitalpurpose.betterworld.server.model.Goal;
import au.com.digitalpurpose.betterworld.server.model.Idea;
import au.com.digitalpurpose.betterworld.server.service.IdeaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/idea")
public class IdeaApi {

    private final IdeaService ideaService;

    @Autowired
    public IdeaApi(IdeaService ideaService) {
        this.ideaService = ideaService;
    }

    @GetMapping()
    public Iterable<Idea> findIdeas(
            @RequestParam(value = "searchTerm", required = false) String searchTerm,
            @RequestParam(value = "goal", required = false) Goal goal
    ) {
        return ideaService.findIdeas(searchTerm, goal);
    }

    @GetMapping("{id}")
    public Idea getIdea(@PathVariable(value = "id") long id) {
        return ideaService.getIdea(id);
    }

    @PostMapping()
    @ResponseBody
    public Idea submitIdea(
            @RequestBody Idea idea
    ) {
        idea.setPublished(java.time.LocalDate.now().toString());
        return ideaService.submitIdea(idea);
    }

}
