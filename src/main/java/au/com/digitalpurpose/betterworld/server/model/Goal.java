package au.com.digitalpurpose.betterworld.server.model;

public enum Goal {
    poverty,
    hunger,
    health,
    education,
    genderEquality,
    sanitation,
    energy,
    economy,
    industry,
    inequality,
    sustainability,
    consumptionProduction,
    climateAction,
    belowWater,
    onLand,
    peace,
    partnerships
}
